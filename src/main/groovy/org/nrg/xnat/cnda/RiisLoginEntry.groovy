package org.nrg.xnat.cnda

import com.fasterxml.jackson.annotation.JsonProperty

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class RiisLoginEntry {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern('yyyy-MM-dd HH:mm:ss.0')

    @JsonProperty('Project ID') String projectId
    @JsonProperty('Login Time') String loginTime
    @JsonProperty('Ae Title') String aeTitle

    LocalDateTime toDateTime() {
        LocalDateTime.parse(loginTime, formatter)
    }

}
