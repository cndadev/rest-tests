package org.nrg.xnat.cnda

import org.apache.commons.lang3.time.StopWatch
import org.nrg.testing.CommonUtils
import org.nrg.testing.enums.TestData
import org.nrg.testing.xnat.BaseXnatRestTest
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.experiments.Scan
import org.nrg.xnat.pogo.users.User

import static org.testng.AssertJUnit.assertEquals

class BaseCndaRestTest extends BaseXnatRestTest {

    public static final String SAMPLE1_SUBJECT_NAME = 'Sample_Patient'
    public static final String SAMPLE1_SESSION_LABEL = 'Sample_ID'
    public static final String PET_SUBJECT_NAME = 'GSU001'
    public static final String PET_SESSION_LABEL = 'GSU001_v00_pet'
    public static final String RECEIVING_STATUS = 'RECEIVING'
    public static final int SAMPLE1_FILES_PER_SCAN = 176
    public static final int SAMPLE1_NUM_SCANS = 3
    public static final int PET_FILES_PER_SCAN = 10
    public static final int PET_NUM_SCANS = 1
    public static final int AUTOARCHIVE_WAIT_TIME = 5
    public static final int AUTOARCHIVE_WAIT_SCHEDULE = 60000
    public static final int ARCHIVE_WAIT_TOLERANCE = 60
    public static final int MAXIMUM_ARCHIVE_WAIT = (60 * AUTOARCHIVE_WAIT_TIME) + (AUTOARCHIVE_WAIT_SCHEDULE / 1000) + ARCHIVE_WAIT_TOLERANCE // convert AUTOARCHIVE_WAIT_TIME from minutes to seconds and AUTOARCHIVE_WAIT_SCHEDULE from milliseconds to seconds
    public static final int MAXIMUM_AUTORUN_TIME = 180

    protected List<PrearchiveEntry> readPrearchiveEntries(User user, Project project) {
        restDriver.interfaceFor(user).jsonQuery().get(formatRestUrl("/prearchive/projects/${project.id}")).then().assertThat().statusCode(200).and().
                extract().jsonPath().getObject('ResultSet.Result', PrearchiveEntry[])
    }

    protected void assertProjectPrearchiveOnlySample1(User user, Project project) {
        final List<PrearchiveEntry> projectEntries = readPrearchiveEntries(user, project)
        assertEquals(1, projectEntries.size())
        final PrearchiveEntry entry = projectEntries.get(0)
        assertEquals(SAMPLE1_SUBJECT_NAME, entry.subject)
        assertEquals(SAMPLE1_SESSION_LABEL, entry.name)
        assertEquals(RECEIVING_STATUS, entry.status)
        assertEquals(TestData.SAMPLE_1.studyInstanceUid, entry.tag)
    }

    protected void scanCheck(List<Scan> scans, int expectedNumScans, int expectedFilesPerScan) {
        assertEquals(expectedNumScans, scans.size())
        scans.each { scan ->
            assertEquals(expectedFilesPerScan, restDriver.findResource(scan.scanResources, 'DICOM').fileCount)
            assertEquals(2, restDriver.findResource(scan.scanResources, 'SNAPSHOTS').fileCount)
        }
    }

    protected void waitForPrearchiveEmpty(Project project) {
        final StopWatch stopWatch = CommonUtils.launchStopWatch()
        while (true) {
            CommonUtils.checkStopWatch(stopWatch, MAXIMUM_ARCHIVE_WAIT, 'Session did not archive in time')
            if (readPrearchiveEntries(mainUser, project).isEmpty()) {
                break
            } else {
                CommonUtils.sleep(1000) // don't pound the server as fast as possible over and over
            }
        }
    }


}
