package org.nrg.xnat.cnda

import com.fasterxml.jackson.annotation.JsonProperty
import org.nrg.xnat.pogo.users.User

class RiisConfig {
    @JsonProperty('Threshold') int threshold
    @JsonProperty('Users') List<String> users = []

    void removeUser(User user) {
        users.remove(user.username)
    }

    void addUser(User user) {
        users << user.username
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (!(o instanceof RiisConfig)) return false

        RiisConfig that = (RiisConfig) o

        if (threshold != that.threshold) return false
        if (users != that.users) return false

        return true
    }

    int hashCode() {
        int result
        result = threshold
        result = 31 * result + (users != null ? users.hashCode() : 0)
        return result
    }

}
