package org.nrg.xnat.cnda.test

import com.fasterxml.jackson.databind.ObjectMapper
import org.dcm4che3.data.Tag
import org.nrg.testing.CommonUtils
import org.nrg.testing.annotations.TestRequires
import org.nrg.testing.enums.TestData
import org.nrg.testing.util.RandomHelper
import org.nrg.testing.xnat.conf.Settings
import org.nrg.xnat.cnda.BaseCndaRestTest
import org.nrg.xnat.cnda.RiisConfig
import org.nrg.xnat.cnda.RiisLoginEntry
import org.nrg.xnat.dicom.CStore
import org.nrg.xnat.enums.PrearchiveCode
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.dicom.DicomScpReceiver
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.experiments.sessions.MRSession
import org.nrg.xnat.pogo.users.User
import org.nrg.xnat.rest.Credentials
import org.nrg.xnat.util.FileIOUtils
import org.nrg.xnat.util.TimeUtils
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.*

import static org.testng.AssertJUnit.*

@SuppressWarnings('FieldCanBeLocal')
@TestRequires(admin = true, data = TestData.SAMPLE_1)
class TestRiisFunctionality extends BaseCndaRestTest {

    private final ObjectMapper objectMapper = new ObjectMapper()
    private final DicomScpReceiver bay2 = new DicomScpReceiver().host(new URL(Settings.BASEURL).getHost()).aeTitle('CNDA_BAY2').port(8104)
    private final DicomScpReceiver bay3 = new DicomScpReceiver().host(new URL(Settings.BASEURL).getHost()).aeTitle('CNDA_BAY3').port(8104)
    private final Project bay3Unassigned = new Project('BAY3_AUTOSEND')
    private final Project unassigned = new Project('Unassigned')
    private final int defaultThreshold = 500
    private final DateTimeFormatter riisLoginFormatter = DateTimeFormatter.ofPattern('MM/dd/uuuu_HH:mm:ss')

    TestRiisFunctionality() throws MalformedURLException {}

    @BeforeMethod(alwaysRun = true)
    void resetThreshold() {
        updateThreshold(defaultThreshold)
    }

    @Test
    void testStandardSessionSend() {
        final Project prearcProject = new Project().prearchiveCode(PrearchiveCode.MANUAL)
        restDriver.createProject(mainUser, prearcProject)
        addUserToRiis(mainUser)
        loginToRiis(mainUser, prearcProject, bay3.getAeTitle())
        CStore.to(bay3).directory(TestData.SAMPLE_1.toDirectory()).headers(currentTimeToDicomStudyDateTime()).send()
        assertProjectPrearchiveOnlySample1(mainUser, prearcProject)
    }

    @Test
    void testSecondFractions() {
        final Project prearcProject = new Project().prearchiveCode(PrearchiveCode.MANUAL)
        restDriver.createProject(mainUser, prearcProject)
        addUserToRiis(mainUser)
        loginToRiis(mainUser, prearcProject, bay3.getAeTitle())

        final LocalDateTime now = LocalDateTime.now()

        CStore.to(bay3).directory(TestData.SAMPLE_1.toDirectory()).headers([(Tag.StudyDate) : now.format(TimeUtils.DICOM_DA_FORMAT), (Tag.StudyTime) : now.format(DateTimeFormatter.ofPattern('HHmmss.123456'))]).send()
        assertProjectPrearchiveOnlySample1(mainUser, prearcProject)
    }

    @Test
    void testUnlistedRiisUser() {
        final int seconds = 3
        CommonUtils.sleep(1000 * seconds) // make sure the latest login time later in the test was at least 3 seconds before the start of the test
        final Project project = new Project()
        restDriver.createProject(mainUser, project)
        removeUserFromRiis(mainUser)
        final LocalDateTime now = LocalDateTime.now()
        Credentials.build(mainUser).queryParam('projectID', project.getId()).queryParam('loginTime', now.format(riisLoginFormatter)).queryParam('aeTitle', bay3.getAeTitle()).
                post(formatRestUrl('services/riis')).then().assertThat().statusCode(401) // this really should be a 403, but eh, whatever
        final RiisLoginEntry recentLoginEntry = readMostRecentRiisLogin()
        assertTrue(recentLoginEntry.toDateTime().isBefore(now.minusSeconds(seconds)))
        assertFalse(project.getId() == recentLoginEntry.getProjectId())
    }

    @Test
    void testInvalidProject() {
        final Project project = new Project()
        final String newLabel = RandomHelper.randomID()

        restDriver.clearPrearchiveSessions(mainAdminUser, bay3Unassigned)
        addUserToRiis(mainUser)
        loginToRiis(mainUser, project, bay3.getAeTitle())
        final Map<Integer, String> headers = currentTimeToDicomStudyDateTime()
        headers.put(Tag.PatientName, newLabel)
        headers.put(Tag.PatientID, newLabel)
        CStore.to(bay3).directory(TestData.SAMPLE_1.toDirectory()).headers(headers).send()
        assertTrue(readPrearchiveEntries(mainAdminUser, unassigned).any { entry ->
            entry.name == newLabel && entry.subject == newLabel
        })
    }

    @Test
    void testThreshold() {
        restDriver.clearPrearchiveSessions(mainAdminUser, bay3Unassigned)
        final Project project = new Project()
        restDriver.createProject(mainUser, project)
        addUserToRiis(mainUser)
        updateThreshold(1)
        loginToRiis(mainUser, project, bay3.getAeTitle())
        CStore.to(bay3).directory(TestData.SAMPLE_1.toDirectory()).headers(timeToDicomStudyDateTime(LocalDateTime.now().plusMinutes(2))).send()
        assertProjectPrearchiveOnlySample1(mainAdminUser, bay3Unassigned)
    }

    @Test
    void testAutoarchiveSend() {
        final Project project = new Project().prearchiveCode(PrearchiveCode.AUTO_ARCHIVE)
        restDriver.createProject(mainUser, project)
        addUserToRiis(mainUser)
        loginToRiis(mainUser, project, bay3.getAeTitle())
        CStore.to(bay3).directory(TestData.SAMPLE_1.toDirectory()).headers(currentTimeToDicomStudyDateTime()).send()
        assertProjectPrearchiveOnlySample1(mainUser, project)
        waitForPrearchiveEmpty(project)
        final Subject generatedSubject = new Subject(project, SAMPLE1_SUBJECT_NAME)
        final ImagingSession generatedSession = new MRSession(project, generatedSubject, SAMPLE1_SESSION_LABEL)
        restDriver.waitForAutoRun(mainUser, MAXIMUM_AUTORUN_TIME, generatedSession)
        scanCheck(restDriver.readScans(mainUser, project, generatedSubject, generatedSession), SAMPLE1_NUM_SCANS, SAMPLE1_FILES_PER_SCAN)
    }

    @Test
    @TestRequires(data = TestData.SIMPLE_PET)
    void testConcurrentScannerUploads() {
        final Project bay2Project = new Project().prearchiveCode(PrearchiveCode.AUTO_ARCHIVE)
        final Project bay3Project = new Project().prearchiveCode(PrearchiveCode.AUTO_ARCHIVE)
        restDriver.createProject(mainUser, bay2Project)
        restDriver.createProject(mainUser, bay3Project)
        addUserToRiis(mainUser)
        loginToRiis(mainUser, bay2Project, bay2.getAeTitle())
        CommonUtils.sleep(2000)
        loginToRiis(mainUser, bay3Project, bay3.getAeTitle())
        final Map<Integer, String> timeHeaders = currentTimeToDicomStudyDateTime()

        final List<File> sample1Files = FileIOUtils.listFilesRecursively(TestData.SAMPLE_1.toDirectory())
        final List<File> petFiles = FileIOUtils.listFilesRecursively(TestData.SIMPLE_PET.toDirectory())
        Math.max(sample1Files.size(), petFiles.size()).times { index ->
            if (index < sample1Files.size()) {
                CStore.to(bay2).file(sample1Files.get(index)).headers(timeHeaders).send()
            }
            if (index < petFiles.size()) {
                CStore.to(bay3).file(petFiles.get(index)).headers(timeHeaders).send()
            }
        }
        waitForPrearchiveEmpty(bay2Project)
        waitForPrearchiveEmpty(bay3Project)
        final Subject bay2GeneratedSubject = new Subject(bay2Project, SAMPLE1_SUBJECT_NAME)
        final ImagingSession bay2GeneratedSession = new MRSession(bay2Project, bay2GeneratedSubject, SAMPLE1_SESSION_LABEL)
        final Subject bay3GeneratedSubject = new Subject(bay3Project, PET_SUBJECT_NAME)
        final ImagingSession bay3GeneratedSession = new MRSession(bay3Project, bay3GeneratedSubject, PET_SESSION_LABEL)
        restDriver.waitForAutoRun(mainUser, MAXIMUM_AUTORUN_TIME, bay2GeneratedSession)
        restDriver.waitForAutoRun(mainUser, 10, bay3GeneratedSession)
        scanCheck(restDriver.readScans(mainUser, bay2Project, bay2GeneratedSubject, bay2GeneratedSession), SAMPLE1_NUM_SCANS, SAMPLE1_FILES_PER_SCAN)
        scanCheck(restDriver.readScans(mainUser, bay3Project, bay3GeneratedSubject, bay3GeneratedSession), PET_NUM_SCANS, PET_FILES_PER_SCAN)
    }

    private RiisConfig queryConfig() {
        final String innerJsonString = restDriver.interfaceFor(mainAdminUser).jsonQuery().get(formatRestUrl('config/riisConfig/config')).then().assertThat().statusCode(200).and().
                extract().jsonPath().getString('ResultSet.Result.get(0).contents')
        try {
            objectMapper.readValue(innerJsonString, RiisConfig.class)
        } catch (IOException e) {
            throw new AssertionError('Failed to read RIIS config object', e)
        }
    }

    private void putConfig(RiisConfig config) {
        restDriver.interfaceFor(mainAdminUser).queryBase().body(config).put(formatRestUrl('config/riisConfig/config')).then().assertThat().statusCode(201)
        assertEquals(config, queryConfig())
    }

    private RiisLoginEntry readMostRecentRiisLogin() {
        return restDriver.interfaceFor(mainAdminUser).jsonQuery().queryParam('count', 1).get(formatRestUrl('services/riis')).then().assertThat().statusCode(200).and().
                extract().jsonPath().getObject('ResultSet.Result.get(0)', RiisLoginEntry.class)
    }

    private void addUserToRiis(User user) {
        final RiisConfig currentConfig = queryConfig()
        if (!currentConfig.getUsers().contains(user.getUsername())) {
            currentConfig.addUser(user)
            putConfig(currentConfig)
        }
    }

    private void removeUserFromRiis(User user) {
        final RiisConfig currentConfig = queryConfig()
        currentConfig.removeUser(user)
        putConfig(currentConfig)
    }

    private void updateThreshold(int threshold) {
        final RiisConfig currentConfig = queryConfig()
        if (currentConfig.getThreshold() != threshold) {
            currentConfig.setThreshold(threshold)
            putConfig(currentConfig)
        }
    }

    private void loginToRiis(User user, Project project, String aeTitle) {
        final LocalDateTime now = LocalDateTime.now()
        final int comparisonThreshold = 10

        Credentials.build(user).queryParam('projectID', project.getId()).queryParam('loginTime', now.format(riisLoginFormatter)).queryParam('aeTitle', aeTitle).
                post(formatRestUrl('services/riis')).then().assertThat().statusCode(200)
        final RiisLoginEntry loginRecord = readMostRecentRiisLogin()
        assertTrue(Math.abs(ChronoUnit.SECONDS.between(now, loginRecord.toDateTime())) < comparisonThreshold)
        assertEquals(project.getId(), loginRecord.getProjectId())
        assertEquals(aeTitle, loginRecord.getAeTitle())
    }

    private Map<Integer, String> timeToDicomStudyDateTime(LocalDateTime time) {
        [(Tag.StudyDate) : time.format(TimeUtils.DICOM_DA_FORMAT), (Tag.StudyTime) : time.format(DateTimeFormatter.ofPattern('HHmmss'))]
    }

    private Map<Integer, String> currentTimeToDicomStudyDateTime() {
        return timeToDicomStudyDateTime(LocalDateTime.now())
    }

}
