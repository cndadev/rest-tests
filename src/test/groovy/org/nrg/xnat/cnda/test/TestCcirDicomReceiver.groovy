package org.nrg.xnat.cnda.test

import org.dcm4che3.data.Tag
import org.nrg.testing.enums.TestData
import org.nrg.testing.xnat.conf.Settings
import org.nrg.xnat.cnda.BaseCndaRestTest
import org.nrg.xnat.cnda.PrearchiveEntry
import org.nrg.xnat.dicom.CStore
import org.nrg.xnat.enums.PrearchiveCode
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.dicom.DicomScpReceiver
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.experiments.sessions.MRSession
import org.testng.annotations.AfterMethod
import org.testng.annotations.BeforeClass
import org.testng.annotations.Test

import static org.testng.AssertJUnit.assertEquals

class TestCcirDicomReceiver extends BaseCndaRestTest {

    private final String ccirProjectId = 'CCIR_99999'
    private final String projectAlias = 'CCIR-99999'
    private final Project testProject = new Project().prearchiveCode(PrearchiveCode.AUTO_ARCHIVE)
    private final DicomScpReceiver ccirReceiver = new DicomScpReceiver().host(new URL(Settings.BASEURL).getHost()).aeTitle('CNDA_CCIR').port(8104)

    TestCcirDicomReceiver() throws MalformedURLException {}

    @BeforeClass
    void addTestProject() {
        restDriver.createProject(mainUser, testProject)
    }

    @AfterMethod(alwaysRun = true)
    void cleanUpTestProject() {
        restDriver.clearProject(mainUser, testProject)
        restDriver.clearPrearchiveSessions(mainUser, testProject)
    }

    @Test
    void testTextFollowingProjectId() {
        performStandardTest("CCIR-^${ccirProjectId}_Test", "${ccirProjectId}_Test", '20170102', '054412')
    }

    @Test
    void testTextFollowingProjectAlias() {
        performStandardTest("CCIR-^${projectAlias} Test", ccirProjectId, '20170102', '054416')
    }

    @Test
    void testHourMinuteTime() {
        performStandardTest("CCIR-^${ccirProjectId}", ccirProjectId, '20170102', '0544')
    }

    @Test
    void testAlmostFullySignificantTime() {
        performStandardTest("CCIR-^${ccirProjectId}_123", "${ccirProjectId}_123", '20170103', '054412.1234')
    }

    @Test
    void testFullySignificantTime() {
        performStandardTest("CCIR-^${ccirProjectId}_M", "${ccirProjectId}_M", '20170103', '054412.123456')
    }

    @Test
    void testOtherPrefix() {
        performStandardTest("Cardiac^${ccirProjectId} Adult", ccirProjectId, '20161103', '121212')
    }

    private void performStandardTest(String studyDescriptionValue, String expectedSubjectLabel, String date, String time) {
        final String expectedSessionLabel = "${date}_${time.replace('.', '_')}"
        final Map<Integer, String> tagMap = [:]
        tagMap.put(Tag.StudyDescription, studyDescriptionValue)
        tagMap.put(Tag.StationName, testProject.getId())
        tagMap.put(Tag.StudyDate, date)
        tagMap.put(Tag.StudyTime, time)
        CStore.to(ccirReceiver).directory(TestData.SAMPLE_1.toDirectory()).headers(tagMap).send()
        final List<PrearchiveEntry> projectEntries = readPrearchiveEntries(mainUser, testProject)
        assertEquals(1, projectEntries.size())
        final PrearchiveEntry entry = projectEntries.get(0)
        assertEquals(expectedSubjectLabel, entry.subject)
        assertEquals(expectedSessionLabel, entry.name)
        assertEquals(RECEIVING_STATUS, entry.status)
        assertEquals(TestData.SAMPLE_1.studyInstanceUid, entry.tag)
        waitForPrearchiveEmpty(testProject)
        final Subject generatedSubject = new Subject(testProject, expectedSubjectLabel)
        final ImagingSession generatedSession = new MRSession(testProject, generatedSubject, expectedSessionLabel)
        restDriver.waitForAutoRun(mainUser, MAXIMUM_AUTORUN_TIME, generatedSession)
        scanCheck(restDriver.readScans(mainUser, testProject, generatedSubject, generatedSession), SAMPLE1_NUM_SCANS, SAMPLE1_FILES_PER_SCAN)
    }

}
